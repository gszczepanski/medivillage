package szczepanski.gerard.medivillage.engine.engine;

import javafx.animation.AnimationTimer;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class Engine {

	private final GameLogic gameLogic;

	public void start() {
		gameLogic.initialize();
		GameLoop loop = new GameLoop();
		loop.start();
	}

	private class GameLoop extends AnimationTimer {

		private long timer = System.currentTimeMillis();
		private int fpsCounter = 1;

		@Override
		public void handle(long now) {
			
			if (fpsCounter % 5 == 0) {
				gameLogic.update();
				gameLogic.draw();
			}

			checkIfSecondPassed();
			fpsCounter++;
			
			if (fpsCounter > 60) {
				fpsCounter = 1;
			}
		}

		private void checkIfSecondPassed() {
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				gameLogic.tickForEverySecond();
			}
		}
	}

}
