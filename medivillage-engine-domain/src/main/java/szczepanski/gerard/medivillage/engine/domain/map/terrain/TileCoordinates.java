package szczepanski.gerard.medivillage.engine.domain.map.terrain;

import com.google.common.base.Preconditions;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.map.utils.Isometric;

import java.awt.*;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
class TileCoordinates {

    private final Point cartesianPoint;
    private final Point isoPoint;
    private final Polygon polygon;

    static TileCoordinates fromPoint(Point cartesianPoint) {
        Preconditions.checkNotNull(cartesianPoint);

        Point isoPoint = isoPoint(cartesianPoint);
        return new TileCoordinates(cartesianPoint, isoPoint, polygonOfIsoPoints(isoPoint));
    }

    private static Point isoPoint(Point cartesianPoint) {
        double isometricY = Isometric.isometricY(cartesianPoint.getX(), cartesianPoint.getY());
        double isometricX = Isometric.isometricX(cartesianPoint.getX(), cartesianPoint.getY());
        return new Point(isometricX, isometricY);
    }

    private static Polygon polygonOfIsoPoints(Point isoPoint) {
        int x = (int) isoPoint.getX();
        int y = (int) isoPoint.getY();

        int[] xPoints = {x, x + (Isometric.TILE_WIDTH / 2), x + Isometric.TILE_WIDTH, x + (Isometric.TILE_WIDTH / 2)};
        int[] yPoints = {y + (Isometric.TILE_HEIGHT / 2), y, y + (Isometric.TILE_HEIGHT / 2), y + Isometric.TILE_HEIGHT};
        return new Polygon(xPoints, yPoints, xPoints.length);
    }


}
