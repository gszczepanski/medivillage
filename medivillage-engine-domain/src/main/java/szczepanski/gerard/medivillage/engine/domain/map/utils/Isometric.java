package szczepanski.gerard.medivillage.engine.domain.map.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Isometric {

	public static final int TILE_WIDTH = 64;
	public static final int TILE_HEIGHT = TILE_WIDTH / 2;

	public static double isometricX(double cartesianX, double cartesianY) {
		return (cartesianX - cartesianY) * TILE_WIDTH / 2;
	}

	public static double isometricY(double cartesianX, double cartesianY) {
		return (cartesianX + cartesianY) * TILE_HEIGHT / 2;
	}

}
