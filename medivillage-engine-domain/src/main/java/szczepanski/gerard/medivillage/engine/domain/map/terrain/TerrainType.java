package szczepanski.gerard.medivillage.engine.domain.map.terrain;

import javafx.scene.image.Image;
import lombok.Getter;

@Getter
public enum TerrainType {
	
	GRASS("/images/tiles/terrain/grass.png", true, 0.0),
	WATER("", false, 10),
	FIELD("", true, - 0.1),
	DIRT("/images/tiles/terrain/dirt.png", true, - 0.1),
	SAND("", true, - 0.2);
	
	private final Image image;
	private final boolean moveable;
	private final double movementCost;
	
	private TerrainType(String imagePath, boolean moveable, double movementCost) {
		this.image = new Image(TerrainType.class.getResourceAsStream(imagePath));
		this.moveable = moveable;
		this.movementCost = movementCost;
	}
	
}
