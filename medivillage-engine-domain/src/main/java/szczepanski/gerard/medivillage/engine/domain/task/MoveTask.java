package szczepanski.gerard.medivillage.engine.domain.task;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import szczepanski.gerard.medivillage.engine.domain.character.GameCharacter;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.map.GameMap;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainMove;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class MoveTask implements Task {
	
	private final GameMap map;
	private final GameCharacter executor;
	private final TerrainTile target;

	@Override
	public void update() {
		Point executorMapPoint = executor.mapPoint();
		TerrainTile currentTile = executor.getCurrentTile();
		TerrainMove nextMove = map.nextMoveForTile(currentTile, target);
		
		if (nextMove == TerrainMove.NO_MOVE) {
			executor.removeTask();
			return;
		}
		
		if (map.isMoveOnTilePossible(executorMapPoint, nextMove)) {
			doMovement(nextMove);
		}
	}

	private void doMovement(TerrainMove nextMove) {
		move(nextMove);
		Point movedSpritePoint = executor.mapPoint();
		TerrainTile movedTile = map.tileOfCart(movedSpritePoint);
		executor.setCurrentTile(movedTile);
	}

	private void move(TerrainMove move) {
		double x = executor.mapPoint().getX() + move.getCurrentTileXOffset();
		double y = executor.mapPoint().getY() + move.getCurrentTileYOffset();
		executor.updateMapPoint(new Point(x, y));
	}
}
