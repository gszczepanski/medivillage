package szczepanski.gerard.medivillage.engine.domain.task;

public interface Task {
	
	void update();
	
}
