package szczepanski.gerard.medivillage.engine.domain.map;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

@Getter
@AllArgsConstructor
public final class Point {
	
	private final double x;
	private final double y;

	public Point toPointWithoutDecimalValues() {
		return new Point((int) x, (int) y);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Point point = (Point) o;
		return Double.compare(point.x, x) == 0 &&
				Double.compare(point.y, y) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	public boolean isFor(double x, double y) {
		return this.x == x && this.y == y;
	}

	public boolean isFor(int x, int y) {
		return (int) this.x == x && (int) this.y == y;
	}
}
