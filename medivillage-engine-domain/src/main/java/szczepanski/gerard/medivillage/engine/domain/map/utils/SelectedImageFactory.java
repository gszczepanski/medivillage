package szczepanski.gerard.medivillage.engine.domain.map.utils;

import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.ColorInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SelectedImageFactory {

	public static Image createSelectedImage(Image source) {
		ColorInput transparent = new ColorInput();
		transparent.setPaint(Color.TRANSPARENT);
		transparent.setX(0);
		transparent.setY(0);
		transparent.setWidth(source.getWidth());
		transparent.setHeight(source.getHeight());
		
		ColorInput colorInput = new ColorInput();
		colorInput.setPaint(Color.GREENYELLOW);
		colorInput.setX(0);
		colorInput.setY(0);
		colorInput.setWidth(source.getWidth());
		colorInput.setHeight(source.getHeight());

		Blend blend = new Blend();
		blend.setMode(BlendMode.SRC_ATOP);
		blend.setTopInput(colorInput);

		Node img = new ImageView(source);
		img.setEffect(blend);

		SnapshotParameters params = new SnapshotParameters();
		params.setFill(Color.TRANSPARENT);
		Image result = img.snapshot(params, null);

		return result;
	}

}
