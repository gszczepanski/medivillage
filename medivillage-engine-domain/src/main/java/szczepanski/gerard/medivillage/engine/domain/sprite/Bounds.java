package szczepanski.gerard.medivillage.engine.domain.sprite;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Bounds {
	
	private final double x;
	private final double y;
	private final double width;
	private final double height;
	
}
