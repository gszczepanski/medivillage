package szczepanski.gerard.medivillage.engine.domain.sprite;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.Setter;
import szczepanski.gerard.medivillage.engine.domain.common.Entity;
import szczepanski.gerard.medivillage.engine.domain.map.DrawableOnMap;
import szczepanski.gerard.medivillage.engine.domain.map.MapObjectHeight;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.map.SelectableOnMap;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;
import szczepanski.gerard.medivillage.engine.domain.map.utils.Isometric;
import szczepanski.gerard.medivillage.engine.domain.map.utils.SelectedImageFactory;

public abstract class Sprite extends Entity implements DrawableOnMap, SelectableOnMap {
	
	private final MapObjectHeight height;
	private Image image;
	private Image selectedImage;
	private Point point;
	private Bounds bounds;
	
	@Getter
	@Setter
	private TerrainTile currentTile;
	
	public Sprite(String id, Image image, MapObjectHeight height, TerrainTile currentTile) {
		super(id);
		this.image = image;
		this.selectedImage = SelectedImageFactory.createSelectedImage(image);
		this.currentTile = currentTile;
		this.height = height;
		this.point = new Point(currentTile.getX(), currentTile.getY());
		updateBounds();
	}
	
	@Override
	public void draw(GraphicsContext context) {
		drawImage(context, image);
	}
	
	@Override
	public void drawSelected(GraphicsContext context) {
		drawImage(context, selectedImage);
	}
	
	private void drawImage(GraphicsContext context, Image img) {
		double positionX = point.getX() + height.getSize();
		double positionY = point.getY() + height.getSize();
		
		context.save();
		context.translate(Isometric.isometricX(positionX, positionY), Isometric.isometricY(positionX, positionY));
		
		context.drawImage(img, positionX, positionY, img.getWidth(), img.getHeight());
		context.restore();
	}
	
	@Override
	public Point mapPoint() {
		return point;
	}
	
	@Override
	public Bounds bounds() {
		return bounds;
	}
	
	public void updateMapPoint(Point point) {
		this.point = point;
		updateBounds();
	}
	
	private void updateBounds() {
		this.bounds = Bounds.builder()
				.x(point.getX() + height.getSize())
				.y(point.getY() + height.getSize())
				.width(image.getWidth())
				.height(image.getHeight())
				.build();
	}
	
}
