package szczepanski.gerard.medivillage.engine.domain.map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MapObjectHeight {
	
	SMALL(0),
	MEDIUM(-1),
	LARGE(-2);
	
	private final int size;
	
}
